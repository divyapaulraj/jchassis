# README #


### What is this repository for? ###


```
#!java

This repo is created with an intention that all the java projects
will be using the common libraries from the chassis project.
```

### How do I get set up? ###


```
#!java

Currently there is no p2 repository to maintain the same.
Hence, users can use the fatjar from chassis and load this as the dependency 
manually.
```


### Contribution guidelines ###


```
#!java

Only the dependencies are added for now. Configuration with templates for these 
dependencies to minimise the repeated configs for each java project needs to be improvized
```


### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact